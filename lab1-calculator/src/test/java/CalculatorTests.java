import com.example.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculatorTests {
    private static Calculator calculator;
    private static CalculatorOperation calculatorOperation;
    

    @Test
    void additionOperation() {
        calculatorOperation = new AddCalculatorOperation();
        calculator=new Calculator(calculatorOperation);
        Assertions.assertEquals(calculator.solve(2,3),5);
        var thrown = Assertions.assertThrows(IllegalArgumentException.class,()->calculator.solve(2,1,4));
        Assertions.assertEquals("Add operation requires exactly two operands.",thrown.getMessage());
    }

    @Test
    void substractOperation() {
        calculatorOperation=new SubtractCalculatorOperation();
        calculator=new Calculator(calculatorOperation);
        Assertions.assertEquals(calculator.solve(2,5),-3);
        var thrown = Assertions.assertThrows(IllegalArgumentException.class,()->calculator.solve(2,1,4));
        Assertions.assertEquals("Subtraction operation requires exactly two operands.",thrown.getMessage());
    }

    @Test
    void multiplicationOperation() {
        calculatorOperation=new MultiplyCalculatorOperation();
        calculator=new Calculator(calculatorOperation);
        Assertions.assertEquals(calculator.solve(2,5),10);
        var thrown = Assertions.assertThrows(IllegalArgumentException.class,()->calculator.solve(2,1,4));
        Assertions.assertEquals("Multiplication operation requires exactly two operands.",thrown.getMessage());
    }

    @Test
    void divisionOperation() {
        calculatorOperation=new DivideCalculatorOperation();
        calculator=new Calculator(calculatorOperation);
        Assertions.assertEquals(calculator.solve(10,5),2);
        var thrown = Assertions.assertThrows(IllegalArgumentException.class,()->calculator.solve(2,1,4));
        Assertions.assertEquals("Division operation requires exactly two operands.",thrown.getMessage());
    }

    @Test
    void minOperation() {
        calculatorOperation=new MinCalculatorOperation();
        calculator=new Calculator(calculatorOperation);
        Assertions.assertEquals(calculator.solve(2,5),2);
        var thrown = Assertions.assertThrows(IllegalArgumentException.class,()->calculator.solve(2,1,4));
        Assertions.assertEquals("Min operation requires exactly two operands.",thrown.getMessage());
    }

    @Test
    void maxOperation() {
        calculatorOperation=new MaxCalculatorOperation();
        calculator=new Calculator(calculatorOperation);
        Assertions.assertEquals(calculator.solve(2,5),5);
        var thrown = Assertions.assertThrows(IllegalArgumentException.class,()->calculator.solve(2,1,4));
        Assertions.assertEquals("Max operation requires exactly two operands.",thrown.getMessage());
    }

    @Test
    void sqrtOperation() {
        calculatorOperation=new SqrtCalculatorOperation();
        calculator=new Calculator(calculatorOperation);
        Assertions.assertEquals(calculator.solve(9),3);
        var thrown = Assertions.assertThrows(IllegalArgumentException.class,()->calculator.solve(2,1));
        Assertions.assertEquals("Square root operation requires exactly one operand.",thrown.getMessage());
    }
}
