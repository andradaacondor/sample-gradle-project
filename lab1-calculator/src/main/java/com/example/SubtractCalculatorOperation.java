package com.example;

public class SubtractCalculatorOperation implements CalculatorOperation {
    @Override
    public double calculate(double... operands) {
        if(operands.length != 2) {
            throw new IllegalArgumentException("Subtraction operation requires exactly two operands.");
        }
        return operands[0] - operands[1];
    }
}
