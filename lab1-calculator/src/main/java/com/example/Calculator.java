package com.example;

public class Calculator {
    private final CalculatorOperation calculatorOperation;

    public Calculator(CalculatorOperation calculatorOperation) {
        this.calculatorOperation = calculatorOperation;
    }

    public double solve(double... operands) {
        return calculatorOperation.calculate(operands);
    }
}
