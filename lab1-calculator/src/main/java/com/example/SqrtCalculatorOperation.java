package com.example;

public class SqrtCalculatorOperation implements CalculatorOperation {
    @Override
    public double calculate(double... operands) {
        if(operands.length != 1) {
            throw new IllegalArgumentException("Square root operation requires exactly one operand.");
        }
        return Math.sqrt(operands[0]);
    }
}
