package com.example;

public class MultiplyCalculatorOperation implements CalculatorOperation {
    @Override
    public double calculate(double... operands) {
        if(operands.length != 2) {
            throw new IllegalArgumentException("Multiplication operation requires exactly two operands.");
        }
        return operands[0] * operands[1];
    }
}
