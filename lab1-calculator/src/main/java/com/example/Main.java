package com.example;

public class Main {
    public static void main(String[] args) {
        if(args.length == 0) {
            System.out.println("No cmd arguments.");
        }

        CalculatorOperation operation = null;
        if(args.length > 0) {
            switch (args[0]) {
                case "add":
                    operation = new AddCalculatorOperation();
                    break;
                case "subtract":
                    operation = new SubtractCalculatorOperation();
                    break;
                case "multiply":
                    operation = new MultiplyCalculatorOperation();
                    break;
                case "divide":
                    operation = new DivideCalculatorOperation();
                    break;
                case "min":
                    operation = new MinCalculatorOperation();
                    break;
                case "max":
                    operation = new MaxCalculatorOperation();
                    break;
                case "sqrt":
                    operation = new SqrtCalculatorOperation();
                    break;
                default:
                    System.out.println("Type an existing operation");
            }
        }

        if(operation != null) {
            Calculator calculator=new Calculator(operation);
            try {
                double[] cmdArguments = new double[args.length-1];
                for(int i=1;i< args.length;i++) {
                    try {
                        cmdArguments[i-1]=Double.parseDouble(args[i]);
                    } catch (NumberFormatException nfe) {
                        System.out.println(nfe.getMessage());
                    }
                }
                double result = calculator.solve(cmdArguments);
                System.out.println(result);
            }
            catch (IllegalArgumentException iae) {
                System.out.println(iae.getMessage());
            }
        }

    }
}