package com.example;

public class MaxCalculatorOperation implements CalculatorOperation {
    @Override
    public double calculate(double... operands) {
        if(operands.length != 2) {
            throw new IllegalArgumentException("Max operation requires exactly two operands.");
        }
        return Math.max(operands[0], operands[1]);
    }
}
