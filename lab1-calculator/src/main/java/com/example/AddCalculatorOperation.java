package com.example;

public class AddCalculatorOperation implements CalculatorOperation {

    @Override
    public double calculate(double... operands) {
        if(operands.length != 2) {
            throw new IllegalArgumentException("Add operation requires exactly two operands.");
        }
        return operands[0] + operands[1];
    }
}
