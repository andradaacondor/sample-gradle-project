package com.example;

public class DivideCalculatorOperation implements CalculatorOperation {

    @Override
    public double calculate(double... operands) {
        if(operands.length != 2) {
            throw new IllegalArgumentException("Division operation requires exactly two operands.");
        }
        return operands[0] / operands[1];
    }
}
