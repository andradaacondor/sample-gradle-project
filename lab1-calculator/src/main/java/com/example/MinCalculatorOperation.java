package com.example;

public class MinCalculatorOperation implements CalculatorOperation {
    @Override
    public double calculate(double... operands) {
        if(operands.length != 2) {
            throw new IllegalArgumentException("Min operation requires exactly two operands.");
        }
        return Math.min(operands[0], operands[1]);
    }
}
