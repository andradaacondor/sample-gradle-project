package com.example.model;

public interface Identifiable {
    int getId();
}
