package com.example.repository;

public interface InMemoryRepository<E> {
    void add(E entity);
    boolean contains(E entity);
    void remove(E entity);
}