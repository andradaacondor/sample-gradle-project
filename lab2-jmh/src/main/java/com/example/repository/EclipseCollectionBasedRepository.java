package com.example.repository;

import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.impl.list.mutable.FastList;

public class EclipseCollectionBasedRepository<E> implements InMemoryRepository<E> {
    MutableList<E> entities;

    public EclipseCollectionBasedRepository() {
        entities = FastList.newList();
    }

    @Override
    public void add(E entity) {
        entities.add(entity);
    }

    @Override
    public boolean contains(E entity) {
        return entities.contains(entity);
    }

    @Override
    public void remove(E entity) {
        entities.remove(entity);
    }
}