package com.example.repository;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectList;

public class FastUtilBasedRepository<E> implements InMemoryRepository<E> {
    ObjectList<E> entities;

    public FastUtilBasedRepository() {
        entities = new ObjectArrayList<>();
    }

    @Override
    public void add(E entity) {
        if (entity == null) {
            throw new IllegalArgumentException("Entity must not be null!");
        }
        entities.add(entity);
    }

    @Override
    public boolean contains(E entity) {
        return entities.contains(entity);
    }

    @Override
    public void remove(E entity) {
        entities.remove(entity);
    }
}
