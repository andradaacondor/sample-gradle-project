package com.example.repository;

import java.util.HashSet;
import java.util.Set;

public class HashSetBasedRepository<E> implements InMemoryRepository<E>{
    Set<E> entities;

    public HashSetBasedRepository() {
        this.entities = new HashSet<>();
    }

    @Override
    public void add(E entity) {
        if (entity == null)
            throw new IllegalArgumentException("Entity must not be null!");

        if(entities.contains(entity)) {
            return;
        }

        entities.add(entity);
    }

    @Override
    public boolean contains(E entity) {
        return entities.contains(entity);
    }

    @Override
    public void remove(E entity) {
        if (entity == null)
            throw new IllegalArgumentException("Entity must not be null!");

        entities.remove(entity);
    }
}
