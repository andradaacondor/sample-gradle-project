package com.example.repository;

import java.util.ArrayList;
import java.util.List;

public class ArrayListBasedRepository<E> implements InMemoryRepository<E> {
    List<E> entities;

    public ArrayListBasedRepository() {
        this.entities = new ArrayList<>();
    }

    @Override
    public void add(E entity) {
        if (entity == null)
            throw new IllegalArgumentException("Entity must not be null!");

        if(entities.contains(entity)) {
            return;
        }

        entities.add(entity);
    }

    @Override
    public boolean contains(E entity) {
        return entities.contains(entity);
    }

    @Override
    public void remove(E entity) {
        if (entity == null)
            throw new IllegalArgumentException("Entity must not be null!");

        entities.remove(entity);
    }
}

