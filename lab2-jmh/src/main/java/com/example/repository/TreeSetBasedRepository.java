package com.example.repository;

import java.util.Set;
import java.util.TreeSet;

public class TreeSetBasedRepository<E> implements InMemoryRepository<E> {
    Set<E> entities;

    public TreeSetBasedRepository() {
        this.entities = new TreeSet<>();
    }

    @Override
    public void add(E entity) {
        if (entity == null)
            throw new IllegalArgumentException("Entity must not be null!");

        if(entities.contains(entity)) {
            return;
        }

        entities.add(entity);
    }

    @Override
    public boolean contains(E entity) {
        return entities.contains(entity);
    }

    @Override
    public void remove(E entity) {
        if (entity == null)
            throw new IllegalArgumentException("Entity must not be null!");

        entities.remove(entity);
    }
}
