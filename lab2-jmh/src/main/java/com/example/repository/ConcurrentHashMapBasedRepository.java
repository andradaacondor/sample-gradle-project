package com.example.repository;

import com.example.model.Identifiable;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepository<E extends Identifiable> implements InMemoryRepository<E> {
    ConcurrentHashMap<Integer,E> entities;

    public ConcurrentHashMapBasedRepository() {
        this.entities = new ConcurrentHashMap<>();
    }

    @Override
    public void add(E entity) {
        if (entity == null)
            throw new IllegalArgumentException("Entity must not be null!");

        entities.put(entity.getId(),entity);
    }

    @Override
    public boolean contains(E entity) {
        return entities.containsKey(entity.getId());
    }

    @Override
    public void remove(E entity) {
        if (entity == null)
            throw new IllegalArgumentException("Entity must not be null!");

        entities.remove(entity.getId());
    }
}
