
package com.example;

import com.example.model.Order;
import com.example.repository.HashSetBasedRepository;
import com.example.repository.InMemoryRepository;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10, time = 1)
@Measurement(iterations = 20, time = 1)
//@Fork(1)
@State(Scope.Benchmark)
public class HashSetBasedRepositoryBenchmark {
    @State(Scope.Thread)
    public static class MyState {
        InMemoryRepository<Order> repository = new HashSetBasedRepository<>();
        int iterations = 100;

        Order order = new Order(150,4000,2);

        @Setup(Level.Trial)
        public void setUp() {
            for (int i = 0; i < iterations; i++) {
                repository.add(new Order(i,i+100,i*3));
            }

            repository.add(order);
        }
    }

    @Benchmark
    public void testAdd(HashSetBasedRepositoryBenchmark.MyState state) {
        state.repository.add(new Order(state.iterations + 1, 999,10 ));
    }

    @Benchmark
    public boolean testContains(HashSetBasedRepositoryBenchmark.MyState state) {
        return state.repository.contains(state.order);
    }

    @Benchmark
    public void testRemove(HashSetBasedRepositoryBenchmark.MyState state) {
        state.repository.remove(state.order);
    }

    public static void main(String[] args) throws RunnerException {

        Options opt = new OptionsBuilder()
                .include(HashSetBasedRepositoryBenchmark.class.getSimpleName())
                .forks(1)
                .build();

        new Runner(opt).run();
    }
}

